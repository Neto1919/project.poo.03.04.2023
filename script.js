let alunos = []
  class Aluno {
    constructor(nome, professor, matricula) {
      this.nome = nome;
      this.professor = professor;
      this.matricula = matricula;
  }
}

function cadastrarAluno() {
  let nome = byId("nomeAluno").value;
  let professor = byId("professorMinistrante").value;
  let matricula = byId("matricula").value;
  let aluno = new Aluno(nome, professor, matricula);
  alunos.push(aluno);
  apresentarAluno();
}

function apresentarAluno() {
  let tabela = byId("alunos");
  let listar = "<ol>"
  for(let aluno of alunos) {
    listar += `<li>${aluno.nome} (${aluno.professor}) (${aluno.matricula})</li>`
  }
  listar += "</ol>"
  tabela.innerHTML = listar;
}

function byId(id) {
  return document.getElementById(id);
}